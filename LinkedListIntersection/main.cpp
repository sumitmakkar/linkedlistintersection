#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

typedef struct node_structure
{
    int                    data;
    struct node_structure *next;
}node_struct;

class Node
{
    private:
        node_struct *head;
        vector<int>  nodesData;
    
        node_struct* createNode(int num)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data = num;
                newNode->next = NULL;
            }
            return newNode;
        }
    
    public:
        Node(vector<int> nData)
        {
            nodesData = nData;
        }
    
        node_struct* createLinkedList()
        {
            int         listSize = (int)nodesData.size();
            node_struct *nodePtr = NULL;
            for(int i = 0 ; i < listSize ; i++)
            {
                node_struct *newNode = createNode(nodesData[i]);
                if(!nodePtr)
                {
                    nodePtr = newNode;
                    head    = newNode;
                }
                else
                {
                    nodePtr->next = newNode;
                    nodePtr       = nodePtr->next;
                }
            }
            return head;
        }
};

class Engine
{
    private:
        node_struct* getIntersectingNodeBetweenListsAndSizes(node_struct *list1 , node_struct *list2 , int l1Size , int l2Size)
        {
            node_struct *biggerList         = l1Size > l2Size ? list1  : list2;
            node_struct *smallerList        = l1Size <= l2Size ? list1  : list2;
            for(int i = 0 ; i < abs(l1Size - l2Size) ; i++)
            {
                biggerList = biggerList->next;
            }
            while (biggerList)
            {
                if(biggerList == smallerList)
                {
                    return biggerList;
                }
                biggerList  = biggerList->next;
                smallerList = smallerList->next;
            }
            return NULL;
        }
    
    public:
        node_struct* mergeList(node_struct *sourceList , node_struct *toBeMergedList)
        {
            node_struct *head = sourceList;
            while (sourceList->next)
            {
                sourceList = sourceList->next;
            }
            sourceList->next = toBeMergedList;
            return head;
        }
    
        int findDataAtIntersectingNodeBetweenLists(node_struct *list1 , node_struct *list2)
        {
            int list1Count = 0;
            int list2Count = 0;
            node_struct *list1Head = list1;
            node_struct *list2Head = list2;
            while (list1 || list2)
            {
                if(list1)
                {
                    list1Count++;
                    list1 = list1->next;
                }
                if(list2)
                {
                    list2Count++;
                    list2 = list2->next;
                }
                
            }
            node_struct *intersectingNode = getIntersectingNodeBetweenListsAndSizes(list1Head , list2Head , list1Count , list2Count);
            return intersectingNode->data;
        }
    
        void display(node_struct *head)
        {
            while (head)
            {
                cout<<head->data<<" ";
                head = head->next;
            }
            cout<<endl;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> list1              = {1,2,3,4};
    vector<int> list2              = {5,6,7};
    vector<int> commonList         = {8,9,10};
    Node        l1                 = Node(list1);
    Node        l2                 = Node(list2);
    Node        common             = Node(commonList);
    node_struct *linkedLinkedList1 = l1.createLinkedList();
    node_struct *linkedLinkedList2 = l2.createLinkedList();
    node_struct *commonLinkedList  = common.createLinkedList();
    
    Engine      e                  = Engine();
    e.mergeList(linkedLinkedList1 , commonLinkedList);
    e.mergeList(linkedLinkedList2 , commonLinkedList);
    cout<<e.findDataAtIntersectingNodeBetweenLists(linkedLinkedList1 , linkedLinkedList2)<<endl;;
    return 0;
}
